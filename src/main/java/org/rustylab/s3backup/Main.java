package org.rustylab.s3backup;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.rustylab.s3backup.aws.AmazonS3Factory;

import com.amazonaws.services.s3.AmazonS3;

/**
 * Main class for CLI backup tool.
 */
public class Main {

    private static DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy_MM");
    private static DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("dd_HH_mm_ss");

    public static void main(String[] args) {
        CliArgParser p = new CliArgParser(args);
        ConfigLoader cl = new ConfigLoader(p.getConfig());
        
        String regionName = p.getRegionName() != null ? p.getRegionName() : cl.getRegionName();
        String accessKey = p.getAccessKey() != null ? p.getAccessKey() : cl.getAccessKey();
        String secretKey = p.getSecretKey() != null ? p.getSecretKey() : cl.getSecretKey();
        String file = p.getFile() != null ? p.getFile() : cl.getFile();
        String bucket = p.getBucket() != null ? p.getBucket() : cl.getBucket();
        String rootDir = p.getRootDir() != null ? p.getRootDir() : cl.getRootDir();

        if (rootDir == null) {
            rootDir = "mybackup";
        }

        String newName = p.getNewName() != null ? p.getNewName() : cl.getNewName();
        if (newName == null) {
            File f = new File(file);
            newName = f.getName();
        }

        AmazonS3 s3client = AmazonS3Factory.create(regionName, accessKey, secretKey);
        LocalDateTime currentDate = LocalDateTime.now();
        String dateFolderName = currentDate.format(dateFormatter);
        String timeFolderName = currentDate.format(timeFormatter);

        File zipfile = new File(ZipUtil.zipFile(file, rootDir));
        s3client.putObject(
                bucket,
                rootDir + "/" + dateFolderName + "/" + newName + "_" + timeFolderName + ".zip",
                zipfile
        );
        zipfile.delete();
    }

}
