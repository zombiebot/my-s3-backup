package org.rustylab.s3backup;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * Command line arguments parser.
 */
public class CliArgParser {

    private static final Logger logger = Logger.getLogger(CliArgParser.class.getName());
    private String[] args = null;
    private Options options = new Options();

    private String config;
    private String accessKey;
    private String secretKey;
    private String regionName;
    private String bucket;
    private String file;
    private String newName;
    private String rootDir;

    public CliArgParser(String[] args) {
        this.args = args;
        options.addOption("h", "help", false, "show help.");
        options.addOption("c", "config", true, "configuration file");
        options.addOption("ak", "access_key", true, "AWS access key");
        options.addOption("sk", "secret_key", true, "AWS secret key");
        options.addOption("r", "region_name", true, "AWS region name");
        options.addOption("b", "bucket", true, "AWS bucket name");
        options.addOption("f", "file", true, "file to backup");
        options.addOption("n", "new_name", true, "file base name in AWS (optional, default current filename)");
        options.addOption("d", "root_dir", true, "root folder name in AWS bucket (optional, default mybackup)");

        parse();
    }

    private void parse() {
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = null;

        try {
            cmd = parser.parse(options, args);

            if (cmd.hasOption("h")) {
                printHelpText();
            }

            if (cmd.hasOption("c")) {
                config = cmd.getOptionValue("c");
            }

            if (cmd.hasOption("ak")) {
                accessKey = cmd.getOptionValue("ak");
            }

            if (cmd.hasOption("sk")) {
                secretKey = cmd.getOptionValue("sk");
            }

            if (cmd.hasOption("r")) {
                regionName = cmd.getOptionValue("r");
            }

            if (cmd.hasOption("b")) {
                bucket = cmd.getOptionValue("b");
            }

            if (cmd.hasOption("f")) {
                file = cmd.getOptionValue("f");
            }

            if (cmd.hasOption("n")) {
                newName = cmd.getOptionValue("n");
            }

            if (cmd.hasOption("d")) {
                rootDir = cmd.getOptionValue("d");
            }
        } catch (ParseException e) {
            logger.log(Level.SEVERE, "Failed to parse comand line properties", e);
        }
    }

    private void printHelpText() {
        HelpFormatter formater = new HelpFormatter();
        formater.printHelp("Main", options);
        System.exit(0);
    }

    public String[] getArgs() {
        return args;
    }

    public Options getOptions() {
        return options;
    }

    public String getConfig() {
        return config;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public String getRegionName() {
        return regionName;
    }

    public String getBucket() {
        return bucket;
    }

    public String getFile() {
        return file;
    }

    public String getNewName() {
        return newName;
    }

    public String getRootDir() {
        return rootDir;
    }

}
